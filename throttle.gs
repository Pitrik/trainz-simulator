include "defaultlocomotivecabin.gs"
include "Cabin.gs"
include "vehicle.gs"
include "Browser.gs"

class Throttle isclass DefaultLocomotiveCabin
{
	CabinControl _throttle;
	Locomotive _loco;
	
	public void Attach(GameObject obj)
	{
		//
	}
	
	public void Init(CabinControl throttle)
	{
		//zde provest ziskani ovladacu z kabiny, neni-li ovladac v kabine obsazen, nastavit danou promennou do null
		_throttle = throttle;		
	}
	
	public void UserSetControl(Locomotive loco, CabinControl cabinControl, float p_value)
	{
		if (_loco == null)
		{
			_loco = loco;
		}
				
		if (cabinControl == _throttle)
		{
			_loco.SetEngineSetting("throttle", p_value);
		}
	}
	
	public void Update(void)
	{
		
	}
};