include "defaultlocomotivecabin.gs"
include "Cabin.gs"
include "vehicle.gs"
include "Browser.gs"

class Compressor isclass DefaultLocomotiveCabin
{
	bool _isCompressorReady;
	bool _isStarted;
	float _pressure;
	CabinControl _compressorPressureIndicator;
	Locomotive _loco;
	
	public float GetPressureParam(string param)
	{
		float pressureMultiplier = 1.0 / (0.145 * 0.0000703);
		float pressureBase = 14.7 * 0.0000703;
  
		return pressureMultiplier * (_loco.GetEngineParam(param) - pressureBase);
	}
	
	public void Attach(GameObject obj)
	{
		//
	}
	
	public void Init(CabinControl compressorPressureIndicator)
	{
		//zde provest ziskani ovladacu z kabiny, neni-li ovladac v kabine obsazen, nastavit danou promennou do null
		Interface.Print("init");
		_compressorPressureIndicator = compressorPressureIndicator;	
		_pressure = 1;
		_compressorPressureIndicator.SetValue(_pressure);
		_isStarted = false;
	}
	
	public void UserSetControl(Locomotive loco, CabinControl cabinControl, float p_value)
	{
		if (_loco == null)
		{
			_loco = loco;
		}
	}
	
	public void Update(void)
	{
		if (_isCompressorReady)
		{
			_compressorPressureIndicator.SetValue(GetPressureParam("main-reservoir-pressure"));
		} 
	}
	
	public bool StartCompressorThread(bool startCompressorCond)
	{
		if(_isStarted)
		{
			return false;
		}
		
		if (!startCompressorCond)
		{
			Interface.Print("Neni splnena podminka startu kompresoru - motor nebezi nebo neni zapnut kompresor");
			return false;
		}
		
		Interface.Print("Spousteni kompresoru");
		_isStarted = true;
		return true;
	}
	
	public void StartCompressorThreadCallback(bool compressorState)
	{
		_isCompressorReady = compressorState;
	}
	
	public CabinControl GetCompressorPressureIndicator(void)
	{
		return _compressorPressureIndicator;
	}
	
	public bool IsCompressorReady(void)
	{
		return _isCompressorReady;
	}
	
	public float GetPressure(void)
	{
		return _pressure;
	}
};

//vlozit do hlavni tridy - thread nelze bez problemu jinde pouzit
//public thread void CompressorStart(CabinControl compressorPressureIndicator)
//    {
//		Interface.Print("zacinam tlakovat");
//		
//		float mainPressure = 900;
//		float pressure = 0;
//
//		while (pressure < mainPressure)
//		{
//			pressure = pressure + 10;
//			compressorPressureIndicator.SetValue(pressure);
//			
//			Sleep(5);
//			
//			Interface.Print("tlakuji");
//		}
//				
//		Interface.Print("ok tlakovani");
//		
//		compressorObject.StartCompressorThreadCallback(true);
//	}