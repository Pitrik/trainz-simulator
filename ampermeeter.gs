include "defaultlocomotivecabin.gs"
include "Cabin.gs"
include "vehicle.gs"
include "Browser.gs"

class Ampermeeter isclass DefaultLocomotiveCabin
{
	CabinControl _ampNeedle;
	Locomotive _loco;
	
	public void Attach(GameObject obj)
	{
		//
	}
	
	public void Init(CabinControl needle)
	{
		//zde provest ziskani ovladacu z kabiny, neni-li ovladac v kabine obsazen, nastavit danou promennou do null
		_ampNeedle = needle;	
		_ampNeedle.SetValue(1);
	}
	
	public void UserSetControl(Locomotive loco, CabinControl cabinControl, float p_value)
	{
		if (_loco == null)
		{
			_loco = loco;
		}
	}
	
	public void Update(void)
	{
		if (_loco == null)
		{
			return;
		}
		
		_ampNeedle.SetValue(_loco.GetEngineParam("current-drawn"));
	}
};