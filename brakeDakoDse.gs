include "defaultlocomotivecabin.gs"
include "Cabin.gs"
include "vehicle.gs"
include "Browser.gs"

class BrakeDakoBse
{
	bool _isBrakeReady;	
	CabinControl _trainBrake;
	CabinControl _brakePipePressureIndicator;
	CabinControl _brakeCylinderPressureIndicator;
	Locomotive _loco;
		
	public void Attach(GameObject obj)
	{
		//
		
		
	}
	
	public float GetPressureParam(string param)
	{
		float pressureMultiplier = 1.0 / (0.145 * 0.0000703);
		float pressureBase = 14.7 * 0.0000703;
  
		return pressureMultiplier * (_loco.GetEngineParam(param) - pressureBase);
	}
	
	public void Init(CabinControl trainBrake, CabinControl brakePipePressureIndicator, CabinControl brakeCylinderPressureIndicator)
	{
		//zde provest ziskani ovladacu z kabiny, neni-li ovladac v kabine obsazen, nastavit danou promennou do null
		_trainBrake = trainBrake;
		_brakePipePressureIndicator = brakePipePressureIndicator;
		_brakeCylinderPressureIndicator = brakeCylinderPressureIndicator;
		
		_brakePipePressureIndicator.SetValue(1);
		_brakeCylinderPressureIndicator.SetValue(1);
		
		_isBrakeReady = false;
	}
		
	public void UserSetControl(Locomotive loco, CabinControl cabinControl, float p_value, bool isCompressorReady)
	{
		if (_loco == null)
		{
			_loco = loco;
		}
		
		if (_trainBrake == null)
		{
			Interface.Print("trainBrake neni definovan");
			return;
		}
		
		if (cabinControl != _trainBrake)
		{
			return;
		}
		
		if (!isCompressorReady)
		{
			Interface.Print("kompresor neni pripraven");
			return;
		}
		
		if (p_value == 0.0) //svih
		{
			//Interface.Print("svih");
			//nic
		}
		else if (p_value > 0.1 and p_value < 0.2) //prebiti 
		{
			//Interface.Print("prebiti");
			//nic
		}
		else if (p_value > 0.2 and p_value < 0.4) //zaver handle of
		{
			//Interface.Print("zaver");
			_loco.SetEngineSetting("train-lap-brake", 4);
		}
		else if (p_value > 0.4 and p_value < 0.5) //snizeni brzdne sily
		{
			//Interface.Print("snizeni");
			_loco.SetEngineSetting("train-lap-brake", 0);
			_isBrakeReady = true;
		}
		else if (p_value > 0.5 and p_value < 0.7) //drzeni brzdne sily (lap)
		{
			//Interface.Print("drzeni");
			_loco.SetEngineSetting("train-lap-brake", 1);
		}
		else if (p_value > 0.7 and p_value < 0.8) //zvyseni brzdne sily
		{
			//Interface.Print("zvyseni");
			_loco.SetEngineSetting("train-lap-brake", 2);
		}
		else if (p_value == 1.0) //rychlobrzda 
		{
			//Interface.Print("rychlobrzda");
			_loco.SetEngineSetting("train-lap-brake", 3);
		}
	}
	
	public void Update(void)
	{
		if (_trainBrake == null or _loco == null)
		{
			return;
		}
		
		float brake = _loco.GetEngineSetting("train-lap-brake");
		if (brake == 0)
		{
			UserSetControl(_loco, _trainBrake, 0.45, true);
			_trainBrake.SetValue(0.45);
		}
		else if (brake == 1)
		{
			UserSetControl(_loco, _trainBrake, 0.6, true);
			_trainBrake.SetValue(0.6);
		}
		else if (brake == 2)
		{
			UserSetControl(_loco, _trainBrake, 0.75, true);
			_trainBrake.SetValue(0.75);
		}
		else if (brake == 3)
		{
			UserSetControl(_loco, _trainBrake, 1, true);
			_trainBrake.SetValue(1);
		}
		else if (brake == 4)
		{
			UserSetControl(_loco, _trainBrake, 0.3, true);
			_trainBrake.SetValue(0.3);
		}
		
		if (_isBrakeReady)
		{
			_brakePipePressureIndicator.SetValue(GetPressureParam("brake-pipe-pressure"));
			_brakeCylinderPressureIndicator.SetValue(GetPressureParam("brake-cylinder-pressure"));
		}
	}
};