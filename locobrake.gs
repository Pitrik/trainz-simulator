include "defaultlocomotivecabin.gs"
include "Cabin.gs"
include "vehicle.gs"
include "Browser.gs"

class LocoBrake isclass DefaultLocomotiveCabin
{
	CabinControl _brake;
	Locomotive _loco;
	
	public void Attach(GameObject obj)
	{
		//
	}
	
	public void Init(CabinControl brake)
	{
		//zde provest ziskani ovladacu z kabiny, neni-li ovladac v kabine obsazen, nastavit danou promennou do null
		_brake = brake;		
	}
	
	public void UserSetControl(Locomotive loco, CabinControl cabinControl, float p_value)
	{
		if (_loco == null)
		{
			_loco = loco;
		}
				
		if (cabinControl == _brake)
		{
			_loco.SetEngineSetting("loco-auto-brake", p_value);
		}
	}
	
	public void Update(void)
	{
		
	}
};