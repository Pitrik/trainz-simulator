include "defaultlocomotivecabin.gs"
include "Cabin.gs"
include "vehicle.gs"
include "Browser.gs"

include "brakeDakoDse.gs"
include "compressor.gs"
include "startstopdieselengine.gs"
include "locobrake.gs"
include "throttle.gs"
include "ampermeeter.gs"

class Loco isclass DefaultLocomotiveCabin
{
	BrakeDakoBse trainBrakeObject;
	StartStopDieselEngine startStopEngineObject;
	Compressor compressorObject;
	LocoBrake locoBrakeObject;
	Throttle throttleObject;
	Ampermeeter ampObject;
	
	public thread void CompressorStart(CabinControl compressorPressureIndicator)
    {
		Interface.Print("zacinam tlakovat");
		
		float mainPressure = 900;
		float pressure = 0;

		while (pressure < mainPressure)
		{
			pressure = pressure + 10;
			compressorPressureIndicator.SetValue(pressure);
			
			Sleep(1);
			
			Interface.Print("tlakuji");
		}
				
		Interface.Print("ok tlakovani");
		
		compressorObject.StartCompressorThreadCallback(true);
	}

	public void Attach(GameObject obj) 
	{ 		
		trainBrakeObject.Attach(obj);
		startStopEngineObject.Attach(obj);
		compressorObject.Attach(obj);
		locoBrakeObject.Attach(obj);
		throttleObject.Attach(obj);
		ampObject.Attach(obj);
	
		inherited(obj); 
	} 

	public void Init(void) 
	{
		trainBrakeObject = new BrakeDakoBse();
		startStopEngineObject = new StartStopDieselEngine();
		compressorObject = new Compressor();
		locoBrakeObject = new LocoBrake();
		throttleObject = new Throttle();
		ampObject = new Ampermeeter();
	
		trainBrakeObject.Init(GetNamedControl("trainbrakelap_lever_custom"), GetNamedControl("bptrainbrakepipe"), GetNamedControl("bptrainbrakecylinder"));
		startStopEngineObject.Init(GetNamedControl("start"), GetNamedControl("stop"));
		compressorObject.Init(GetNamedControl("bplocomain"));
		locoBrakeObject.Init(GetNamedControl("independantbrake_lever"));
		throttleObject.Init(GetNamedControl("throttle_lever"));
		ampObject.Init(GetNamedControl("ampmeter"));
		
		inherited();
	}
	
	public void UserSetControl(CabinControl p_control, float p_value) 
	{ 
		//obsluha startu motoru disloveho motoru lokomotivy
		startStopEngineObject.UserSetControl(loco, p_control, p_value);
		
		//obsluha kompresoru - v tomto pripade se kompresor spousti po startu motoru lokomotivy
		compressorObject.UserSetControl(loco, p_control, p_value);
		
		bool isStart = compressorObject.StartCompressorThread(startStopEngineObject.IsEngineStarted());
		if (isStart)
		{
			CabinControl indicator = compressorObject.GetCompressorPressureIndicator();
			CompressorStart(indicator);
		}
		
		//obsluha brzdice - v tomto pripade lze brzdic obsluhovat po dosazeni provozniho tlaku v hlavni jimce
		trainBrakeObject.UserSetControl(loco, p_control, p_value, compressorObject.IsCompressorReady());
		
		//obsluha tahu
		throttleObject.UserSetControl(loco, p_control, p_value);
		
		//obsluha lokomotivni brzdy
		locoBrakeObject.UserSetControl(loco, p_control, p_value);
		
		//ampermetr
		ampObject.UserSetControl(loco, p_control, p_value);
	}

	public void Update(void)
	{
		if (!loco)
		{
			return;
		}
						
		trainBrakeObject.Update();
		startStopEngineObject.Update();
		compressorObject.Update();
		throttleObject.Update();
		locoBrakeObject.Update();
		ampObject.Update();
				
		int position = loco.GetEngineSetting("throttle");	
		
		SetFXNameText("ups","" + HTMLWindow.PadZerosOnFront(position,2));
    	SetMeshAnimationFrame("nochi_posi",position*5);

		int real_speed = loco.GetVelocity() * 3.6 + 0.4;
		SetFXNameText("rsp", "" + HTMLWindow.PadZerosOnFront(real_speed,3));

		int max_speed = loco.GetMyTrain().GetSpeedLimit() * 3.6 + 0.4;

		SetFXNameText("msp", "" + HTMLWindow.PadZerosOnFront(max_speed,3));
		int rspp = loco.GetMyTrain().GetSpeedLimit() * 3.6 + 0.4;
    	SetMeshAnimationFrame("speedo_limit",rspp/1.55);

		SetFXNameText("time_cur","" + HTMLWindow.GetFloatAsTimeString(World.GetGameTime()));
		
		inherited();
	}
};
