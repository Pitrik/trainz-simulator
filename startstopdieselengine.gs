include "defaultlocomotivecabin.gs"
include "Cabin.gs"
include "vehicle.gs"
include "Browser.gs"

class StartStopDieselEngine isclass DefaultLocomotiveCabin
{
	bool _engineStarted;
	CabinControl _startEngine;
	CabinControl _stopEngine;
	Locomotive _loco;
	
	public void Attach(GameObject obj)
	{
		//
	}
	
	public void Init(CabinControl startEngine, CabinControl stopEngine)
	{
		//zde provest ziskani ovladacu z kabiny, neni-li ovladac v kabine obsazen, nastavit danou promennou do null
		_startEngine = startEngine;
		_stopEngine = stopEngine;		
	}
	
	public void UserSetControl(Locomotive loco, CabinControl cabinControl, float p_value)
	{
		if (_loco == null)
		{
			_loco = loco;
		}
		
		if (_stopEngine == null or _startEngine == null)
		{
			Interface.Print("loko nema definovan start nebo stop motoru");
			_engineStarted = true;
			
			return;
		}
		
		if (cabinControl == _startEngine)
		{
			_engineStarted = true;
		}
		else if (cabinControl == _stopEngine)
		{
			_engineStarted = false;
		}
	}
	
	public void Update(void)
	{
		
	}
	
	public bool IsEngineStarted(void)
	{
		return _engineStarted;
	}
};